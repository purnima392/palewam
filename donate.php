<?php include('header.php')?>
<section class="content inner-content">
	<div class="container">
		<div class="row mb-20">
			<div class="col">
				<h2 class="innertitle">How you can help </h2>
			</div>
			<div class="col text-right"> <a href="index.php" class="breadcrumb"> <i class="fa fa-home"></i> Back to home </a> </div>
		</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>S.N.</th>
					<th>Expenditure Head</th>
					<th>Monthly US$</th>
					<th>Yearly US$</th>
					<th>Amt. in NPRs</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1.</td>
					<td>Schooling & Fooding</td>
					<td>US$ 45</td>
					<td>US$ 540</td>
					<td>Rs 54,000/-</td>
					
				</tr>
				<tr>
					<td>2.</td>
					<td>Stationary</td>
					<td>US$ 5</td>
					<td>US$ 60</td>
					<td>Rs 6,000/-</td>
					
				</tr>
				<tr>
					<td>3.</td>
					<td>Medication</td>
					<td>US$ 3</td>
					<td>US$ 36</td>
					<td>Rs 3,600/-</td>
					
				</tr>
				<tr>
					<td>4.</td>
					<td>Clothing</td>
					<td>US$ 6</td>
					<td>US$ 72</td>
					<td>Rs 7,200/-</td>
					
				</tr>
				<tr>
					<td>5.</td>
					<td>Toiltries</td>
					<td>US$ 2</td>
					<td>US$ 24</td>
					<td>Rs 2,400/-</td>
					
				</tr>
				<tr>
					<td>6.</td>
					<td>Monk’s sets</td>
					<td>US$ 2</td>
					<td>US$ 24</td>
					<td>Rs 2,400/-</td>
					
				</tr>
				<tr>
					<td></td>
					<td><b>Total</b></td>
					<td>US$ 63</td>
					<td>US $ 756</td>
					<td>Rs 75,600/-</td>
					
				</tr>
				
			</tbody>
		</table>
		<p>If you wish to sponsor our child, this is a costing of individual child which cost 756 US$ per year. Or you can help us in any expenditure heading for a child. </p>
		<p><b>For the Donation use below Bank address:</b></p>
		<ul class="donation-address">
			<li><b>Account Name: </b> Pal Ewam Namgyal Monastic School</li>
			<li><b>Account Number: </b> 11-22-524-282277-01-4</li>
			<li><b>Swift Code:  </b>MBLNNPKA</li>
			<li><span>Macchapuchchre Bank Ltd</span><span>Nayabazar, Pokhara, Nepal</span><span><a href="#">www.machbank.com</a></span></li>
		</ul>
		<hr>
		<h2 class="innertitle">Donation Form Pledge</h2>
		<div class="contact-form donation-form">
			
			<form class="form-horizontal">
				<div class="form-group">
					<label for="name">Name :</label>
					<input type="text" class="form-control" id="name">
				</div>
				<div class="form-group">
					<label for="address">Address :</label>
					<input type="text" class="form-control" id="address">
				</div>
				
				<div class="form-group">
					<label for="phonenumber">Phone Number :</label>
					<input type="text" class="form-control" id="phonenumber">
				</div>
				<div class="form-group">
					<label for="email">Email :</label>
					<input type="text" class="form-control" id="email">
				</div>
				<div class="form-group">
					<label for="email">Email :</label>
					<input type="email" class="form-control" id="email">
				</div>
				<div class="form-group"><button type="submit" class="btn btn-primary">Submit</button></div>
			</form>
		</div>
	</div>
</section>
<?php include('footer.php')?>