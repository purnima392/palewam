<?php include('header.php');
      require('AdminLTE/inc/config.php');
?>
<html>
  <head>
    <meta name="generator"
    content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
    <title></title>
  </head>
  <body>
    <section class="content inner-content">
      <div class="container">
        <div class="row mb-20">
          <div class="col">
            <h2 class="innertitle">Articles/Blogs</h2>
          </div>
          <div class="col text-right">
            <a href="index.php" class="breadcrumb">Back to home</a>
          </div>
        </div>
        <div class="row">
	<?php
            $latProjects=$mysqli->query("SELECT *, count( cm.comment ) AS CommentsCount FROM blog bg
                                         LEFT JOIN comments cm ON bg.BlogId = cm.BlogId
                                          GROUP BY bg.BlogId");
            while($SiPackage=$latProjects->fetch_array()){
		$BlogId=$SiPackage["BlogId"];
		$Title=$SiPackage["Title"];
		$Description=$SiPackage["Description"];
		$Photo=$SiPackage["Photo"];
    $CreatedOn=$SiPackage["CreatedOn"];
    $Count=$SiPackage["CommentsCount"];
	  ?>
          <div class="col-lg-3 col-md-3">
            <article class="blogpost">
              <div class="blog-img">
                <img src="img/<?=$Photo?>" />
              </div>
              <div class="blogpost-body">
                <div class="blog-title">
                  <h2 class="title">
                    <a href="blog-detail.php?id=<?=$BlogId?>"><?=$Title?></a>
                  </h2>
                  <div class="post-info">
                    <span><?=$CreatedOn?></span>
                  </div>
                 <!--  <div class="submitted">by 
                  <a href="#">India</a></div> -->
                </div>
                <div class="blogpost-content">
                  <div class="Description"><?=$Description?></div>
                </div>
                <div class="blog-footer clearfix">
                  <div class="pull-left">
                    <a href="#"><?=$Count?> comments</a>
                  </div>
                  <div class="blog-btn pull-right">
                    <a href="blog-detail.php?id=<?=$BlogId?>" class="links">Read More</a>
                  </div>
                </div>
              </div>
            </article>
          </div>
          
	  <?php } ?>
        </div>
      </div>
    </section><?php include('footer.php')?>
  </body>
</html>
