-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2017 at 12:36 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_palewam`
--

-- --------------------------------------------------------

--
-- Table structure for table `academics`
--

CREATE TABLE IF NOT EXISTS `academics` (
  `AcademicId` int(11) NOT NULL AUTO_INCREMENT,
  `Title` text NOT NULL,
  `Description` text NOT NULL,
  `Photo` varchar(256) NOT NULL,
  PRIMARY KEY (`AcademicId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `academics`
--

INSERT INTO `academics` (`AcademicId`, `Title`, `Description`, `Photo`) VALUES
(1, 'Co-curriculum Activites ', '<p>Every Saturday we organize a\r\ninter class and inter house Tri-lingual Spelling contest, Debate, Quiz,\r\nElocution and Poem Recitation. The motive of organizing such activities is to\r\nmake confident in the public, reading, speaking etc. Students are making a huge\r\nprogress and develop after every Saturday. Sometime we call a special guest\r\njudge from outside to observe our student knowledge and their creativity.\r\nLikewise, students from higher class are getting opportunity to watch different\r\ndocumentary movies in our conference through projector. </p>\r\n\r\n\r\n\r\n\r\n\r\n<br>', '1504607153g.jpg'),
(2, 'Exam', '<p>In a year we have three terminal\r\nexams after every four months. Each term exam is very important for students to\r\nmove in their next grade. Besides the term exam, our teacher will also take a\r\nmonthly and weekly test in their class to check the student capability. It is\r\nnot only to check the students knowledge but also gives a idea to teacher that\r\nhow teaching method should implement to each students. </p><br>', '1504608648sc.jpg'),
(3, 'Computer Class', '<p>In this advanced era computer is\r\na major tools to operate every function. In our school we are giving computer\r\nclass to the senior students. Although we have limited computers now but in the\r\nfuture we are planning to add more if we find any donors. Basics computer like\r\nMS Word, Typing and Excel are taught by our teachers. </p><br>', '1504608743sc.jpg'),
(4, 'Games and Sports', '<p>In our school we are trying to\r\nmake best shape for our students internally as well as from physically. Every\r\nearly morning student has to do a morning workout and exercise for half an\r\nhour. At the same time different games are organize during the weekend.\r\nBasketball match, volley match and football match are organize between inter\r\nhouse to make our physical fit and strong. Sometime our senior students are\r\nsent you other schools for games competition. </p>\r\n\r\n\r\n\r\n\r\n\r\n <br>', '1504608853beds-prep-1-resize-1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `BlogId` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(5000) NOT NULL,
  `Description` varchar(40000) NOT NULL,
  `Photo` varchar(256) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`BlogId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`BlogId`, `Title`, `Description`, `Photo`, `CreatedOn`) VALUES
(2, 'Fundraiser update ', '<div>\r\n              <p>Over 70% of target raised â€“ what an amazing response by our friends and supporters,  Thank you all so very much.</p>\r\n			  <p>Iâ€™ve been asked by several supporters for an update on progress \r\ntowards raising the money for buying much needed bunk beds and bedding \r\nfor our dear girls in their new winter school, so unusually Iâ€™m posting \r\nanother blog this month.</p>\r\n			  <p>Weâ€™re so grateful to all the contributions generously made so \r\nfar.  Between us weâ€™ve raised over 3,300 Euros towards our 5,000 target.\r\n  The great news is that weâ€™ve almost raised enough to pay for the bunk \r\nbeds â€“ please help us reach the target soon so we can buy bedding too.  \r\n We heard from school staff that winter nights are very cold now in \r\nNepal so we felt a great urgency to help the girls off the floor and \r\ninto beds as soon as we could.  So the order for the beds has already \r\nbeen placed with the manufacturer and weâ€™re hoping for delivery soon.</p>\r\n			  <p>Manufacturing progress  is going well â€“ many of the beds have been built and are being painted now.</p>\r\n			  \r\n			  <p>We also need funds to pay for 58 sets of bedding too as the \r\nfloor mattresses wont fit the beds.  So if any of you, dear supporters, \r\n have friends or family members that may also be interested in \r\ncontributing please can I ask you to share this information with them?</p>\r\n			  <p>Some supporters have asked how far their $$$, â‚¬â‚¬â‚¬ or Â£Â£Â£ will go\r\n â€“ and the answer is quite a long way in Nepal!  All donations will be \r\nvery gratefully received â€“ a gift of 5 Euro will buy a pillow and cover,\r\n 15 Euro will help us buy a warm blanket and 30 Euro a mattress.  And \r\naround 80 Euro will fund a bunk bed for 2 girls and 60 Euro will buy a \r\nsingle bed for teachers or support staff.</p>\r\n			  \r\n            </div>\r\n\r\n<br>', '1504603543project.jpg', '2017-09-06 10:07:55'),
(4, 'New Winter School Opening', 'Last month we attended an amazing opening ceremony and inauguration of the new winter school building in Pokhara.When we first opened the school in 2012 with 18 girls it was quite easy to afford to rent a property in Pokhara so the girls and staff could relocate from Upper Mustang,  where it becomes too cold to study in the harsh winter conditions.', '1504603543project.jpg', '2017-09-06 10:17:15'),
(5, 'Mixed News from Nepal', 'As the girls and staff travel down from Mustang to Pokhara for winter school, there is mixed news from Nepal. <br>', '1504603543project.jpg', '2017-09-06 10:18:26');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `CommentId` int(11) NOT NULL AUTO_INCREMENT,
  `Comment` varchar(10000) NOT NULL,
  `BlogId` int(11) NOT NULL,
  `CommentedBy` varchar(10000) NOT NULL,
  `Created_Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Subject` varchar(1000) NOT NULL,
  PRIMARY KEY (`CommentId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`CommentId`, `Comment`, `BlogId`, `CommentedBy`, `Created_Date`, `Subject`) VALUES
(1, 'test message', 2, 'Dipika', '2017-09-07 10:07:32', 'test'),
(2, 'test message', 2, 'Dipika', '2017-09-07 10:07:44', 'test'),
(3, 'test message', 2, 'Dipika', '2017-09-07 10:07:59', 'test'),
(4, 'test message', 2, 'Dipika', '2017-09-07 10:10:23', 'test'),
(5, 'sdf', 2, 'Ceetu', '2017-09-07 10:11:29', 'hel');

-- --------------------------------------------------------

--
-- Table structure for table `cp_admin`
--

CREATE TABLE IF NOT EXISTS `cp_admin` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(256) NOT NULL,
  `Password` varchar(256) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cp_admin`
--

INSERT INTO `cp_admin` (`Id`, `Username`, `Password`) VALUES
(1, 'dipika', 'agrawal');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `GalleryId` int(11) NOT NULL AUTO_INCREMENT,
  `Photo` varchar(256) NOT NULL,
  PRIMARY KEY (`GalleryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`GalleryId`, `Photo`) VALUES
(4, '136.jpg'),
(5, '139.jpg'),
(6, '1504608648sc.jpg'),
(7, '1504608743sc.jpg'),
(8, '1504608853beds-prep-1-resize-1.jpg'),
(9, '1.jpg'),
(10, '2.jpg'),
(11, '4.jpg'),
(12, '5.jpg'),
(13, '6.jpg'),
(14, '7.jpg'),
(15, '8.jpg'),
(16, '9.jpg'),
(17, '10.jpg'),
(18, '11.jpg'),
(19, '12.jpg'),
(20, '13.jpg'),
(21, '14.jpg'),
(22, '15.jpg'),
(23, '16.jpg'),
(24, '17.jpg'),
(25, '18.jpg'),
(26, '19.jpg'),
(27, '20.jpg'),
(28, '21.jpg'),
(29, '22.jpg'),
(30, '23.jpg'),
(31, '24.jpg'),
(32, '25.jpg'),
(33, '26.jpg'),
(34, '27.jpg'),
(35, '28.jpg'),
(36, '29.jpg'),
(37, '1069185_161929867335942_2108011089_n.jpg'),
(38, '20431315_694101650785425_3573674295327555940_n.jpg'),
(39, 'a.jpg'),
(40, 'b.jpg'),
(41, 'c.jpg'),
(42, 'e.jpg'),
(43, 'i.jpg'),
(44, 'n.jpg'),
(45, 'q.jpg'),
(46, 'r.jpg'),
(47, 't.jpg'),
(48, 'u.jpg'),
(49, 'v.jpg'),
(50, 'w.jpg'),
(51, 'x.jpg'),
(52, 'y.jpg'),
(53, 'z.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `ProjectId` int(11) NOT NULL AUTO_INCREMENT,
  `ProjectName` varchar(5000) NOT NULL,
  `Description` text NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `Photo` varchar(256) NOT NULL,
  PRIMARY KEY (`ProjectId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`ProjectId`, `ProjectName`, `Description`, `CategoryId`, `Photo`) VALUES
(8, 'Namgyal Monastery Re-Construction Project', '<p>After the devastating earth hit in Nepal on 25th\r\nApril and 2nd May 2015, thousand of houses and hundreds of people\r\nwere killed in Nepal.  The whole nation\r\nwas filled with tears and shocked. Many people were homeless and several\r\nchildren were orphanage after the destructive earthquake. Likewise, in Upper\r\nMustang Namgyal Monastery was badly harmed by the earthquake. The whole\r\nmonastery was collapsed and in some areas huge cracked were made. And it was a\r\nhuge loss of the local people near our monastery because the Namgyal Monastery\r\nis one of the oldest and holiest monasteries in Nepal. </p>\r\n\r\n<p>Since the Khenpo Tsewang La took an initiative step to\r\nreconstruct the monastery and the project was started on 25th April\r\n2016. With the help of Dharma friends and well wishers around the globe our\r\nproject has completed around 70% and still few major works are left. Painting\r\ninside the monastery, 6.5 Maitreya Buddha Statue and furniture work inside the\r\nmain shrine hall is left. We still are raising fund for these installment. </p>\r\n\r\n\r\n\r\n\r\n\r\n<br>', 1, '15050352385.jpg'),
(9, 'Shrine Hall in Nunnery School (Threngkar)', '<p>Pal Ewam Namgon Nunnery School is\r\nsituated in Upper Mustang, Threngkar. Currently we have 50 nuns with 6 teaching\r\nstaffs. All the nuns are from poor family background from most remote areas of\r\nNepal. During the summer season we have to run our school in Upper Mustang and\r\nduring the winter season we have run our school in Pokhara. Because of the cold\r\nclimatic condition in Upper Mustang during the winter season students are not\r\nable to go class and run the school smoothly. </p>\r\n\r\n<p>So coming year on 2017, we are\r\nplanning to construct a new shrine hall in Nunnery school, Threngkar. Every\r\nyear we organize a Mani programme in Nunnery school and we manage it in our\r\nschool compound with a tent. And itÃ¢â‚¬â„¢s a very tough and difficult because of the\r\nstrong wind there. At the same time every morning and evening our nuns have to\r\ndo their daily puja and now we donÃ¢â‚¬â„¢t have any shrine hall to do puja. So\r\nregarding all these weakness we are trying to construct the shrine hall very\r\nsoon. The cost of the project is 1, 30,000 US $. </p><br>', 2, '150503552718.jpg'),
(10, 'Additional Classroom building in Nunnery school', '<p>With the help from Love @ Pal\r\nrecently, we have build a new classroom building in our Nunnery school in\r\nPokhara. Currently, we have only 5 classrooms but the normal average of room we\r\ndonÃ¢â‚¬â„¢t is 8. Starting from Lower kindergarten to grade 7, we need 8 classrooms.\r\nSo right now we are short of 3 rooms. So we are planning to add another\r\nbuilding in order to level up and manage our classroom equally. </p>\r\n\r\n<p>So we strongly urge to all our\r\ndear friends and well wishers to kindly kelp us for this project. </p><br>', 3, '150503570523.jpg'),
(11, 'Pal Ewam Namgon Meditation Center', '<p>Since our school is a monastic\r\nand it is more related with the religious activities. So many of our friends\r\nand volunteers around the globe wish to stay for meditation and several\r\nsuggestion and opinions were forward to us about the meditation center to\r\nestablish in our Nunnery school. </p>\r\n\r\n<p>So we are targeting our\r\nmeditation center very soon if we get a sponsors and donors for this noble\r\nproject. We already have prepared a draft sketch of meditation center. The\r\ncoast of this project if 79,000 US $. </p><br>', 3, '150503582728.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE IF NOT EXISTS `results` (
  `ResultId` int(11) NOT NULL AUTO_INCREMENT,
  `SymbolNo` varchar(256) NOT NULL,
  `Filename` varchar(256) NOT NULL,
  PRIMARY KEY (`ResultId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `StaffId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(256) NOT NULL,
  `Designation` varchar(256) NOT NULL,
  `Photo` varchar(256) NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `SubCategory` varchar(256) NOT NULL,
  PRIMARY KEY (`StaffId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`StaffId`, `Name`, `Designation`, `Photo`, `CategoryId`, `SubCategory`) VALUES
(26, 'Khenrab Sangbo', 'Chairman', '1505465869khenrab_sangbo(chairman).jpg', 1, ''),
(27, 'J.B. Kunwar', 'Nepali Teacher', '1505466756jb_kunwar.jpg', 2, 'Monastic'),
(28, 'Tanding Tsering', 'Head Master', '1505466811tanding_tsering.jpg', 2, 'Monastic'),
(29, 'Tenzin Dharpo', 'English Teacher', '1505466856tenzin_dharpo.jpg', 2, 'Monastic'),
(31, 'Lobsang Tsedar', 'Cook', '1505466975lopsang_tsedar.jpg', 3, 'Monastic'),
(32, 'Maya B.K.', 'Dish Washer', '1505467029maya_bk.jpg', 3, 'Monastic'),
(33, 'Sonam Choemphel', 'Cook', '1505467063sonam_choemphel.jpg', 3, 'Monastic'),
(34, 'Tsering Namgyal Thakuri', 'Store Incharge', '1505467107tsering_namgyal_thakuri(store-incharge).jpg', 3, 'Monastic'),
(35, 'Karchung Mentok', 'English Teacher', '1505467331karchung_nentok.jpg', 2, 'Nunnery'),
(36, 'Maina Kumari', 'Nepali Teacher', '1505467384maina_kumari_gharti_magar.jpg', 2, 'Nunnery'),
(37, 'Tsering Youdon', 'Maths Teacher', '1505467457tsering_youton.jpg', 2, 'Nunnery'),
(38, 'Yangchen Dolker', 'Social Teacher', '1505467511yangchen_dolker.jpg', 2, 'Nunnery'),
(39, 'Yangchen Gurung', 'Science Teacher', '1505467556yangchen_gurung.jpg', 2, 'Nunnery'),
(40, 'Dawa Tsring Gurung', 'Cook', '1505467604dawa_tsering.jpg', 3, 'Nunnery'),
(41, 'Khenpo Tsewang Rigzin', 'Founder/Director', '15054677461069185_161929867335942_2108011089_n.jpg', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `VideoId` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(256) NOT NULL,
  `VideoLink` varchar(256) NOT NULL,
  PRIMARY KEY (`VideoId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`VideoId`, `Title`, `VideoLink`) VALUES
(1, 'Bepanha pyar', 'KxCjVIFxZNo'),
(2, 'Kal ho na ho', '1BWdglekty0');

-- --------------------------------------------------------

--
-- Table structure for table `volunteers`
--

CREATE TABLE IF NOT EXISTS `volunteers` (
  `VolunteerId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(256) NOT NULL,
  `Age` int(11) NOT NULL,
  `Sex` varchar(256) NOT NULL,
  `Mobile` varchar(256) NOT NULL,
  `Landline` varchar(256) NOT NULL,
  `Email` varchar(256) NOT NULL,
  `SocialLink` varchar(256) NOT NULL,
  `Sponsor` varchar(256) NOT NULL,
  `Contact` varchar(256) NOT NULL,
  `ArrivalDate` datetime NOT NULL,
  `DepartureDate` datetime NOT NULL,
  `Days` int(11) DEFAULT NULL,
  `Weeks` int(11) DEFAULT NULL,
  `AdditionalDays` int(11) DEFAULT NULL,
  `AdditionalWeeks` int(11) DEFAULT NULL,
  `FoodFacility` varchar(256) NOT NULL,
  `RoomFacility` varchar(256) NOT NULL,
  `Internet` varchar(256) NOT NULL,
  `SpecialSkills` varchar(10000) NOT NULL,
  `Photo` varchar(256) NOT NULL,
  `FoodRemarks` varchar(1000) DEFAULT NULL,
  `RoomRemarks` varchar(1000) DEFAULT NULL,
  `WifiRemarks` varchar(1000) DEFAULT NULL,
  `Address` varchar(256) NOT NULL,
  PRIMARY KEY (`VolunteerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

