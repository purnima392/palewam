-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2017 at 12:21 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_palewam`
--

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `StaffId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(256) NOT NULL,
  `Designation` varchar(256) NOT NULL,
  `Photo` varchar(256) NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `SubCategory` varchar(256) NOT NULL,
  PRIMARY KEY (`StaffId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`StaffId`, `Name`, `Designation`, `Photo`, `CategoryId`, `SubCategory`) VALUES
(26, 'Khenrab Sangbo', 'Chairman', '1505465869khenrab_sangbo(chairman).jpg', 1, ''),
(27, 'J.B. Kunwar', 'Nepali Teacher', '1505466756jb_kunwar.jpg', 2, 'Monastic'),
(28, 'Tanding Tsering', 'Head Master', '1505466811tanding_tsering.jpg', 2, 'Monastic'),
(29, 'Tenzin Dharpo', 'English Teacher', '1505466856tenzin_dharpo.jpg', 2, 'Monastic'),
(31, 'Lobsang Tsedar', 'Cook', '1505466975lopsang_tsedar.jpg', 3, 'Monastic'),
(32, 'Maya B.K.', 'Dish Washer', '1505467029maya_bk.jpg', 3, 'Monastic'),
(33, 'Sonam Choemphel', 'Cook', '1505467063sonam_choemphel.jpg', 3, 'Monastic'),
(34, 'Tsering Namgyal Thakuri', 'Store Incharge', '1505467107tsering_namgyal_thakuri(store-incharge).jpg', 3, 'Monastic'),
(35, 'Karchung Mentok', 'English Teacher', '1505467331karchung_nentok.jpg', 2, 'Nunnery'),
(36, 'Maina Kumari', 'Nepali Teacher', '1505467384maina_kumari_gharti_magar.jpg', 2, 'Nunnery'),
(37, 'Tsering Youdon', 'Maths Teacher', '1505467457tsering_youton.jpg', 2, 'Nunnery'),
(38, 'Yangchen Dolker', 'Social Teacher', '1505467511yangchen_dolker.jpg', 2, 'Nunnery'),
(39, 'Yangchen Gurung', 'Science Teacher', '1505467556yangchen_gurung.jpg', 2, 'Nunnery'),
(40, 'Dawa Tsring Gurung', 'Cook', '1505467604dawa_tsering.jpg', 3, 'Nunnery'),
(41, 'Khenpo Tsewang Rigzin', 'Founder/Director', '15054677461069185_161929867335942_2108011089_n.jpg', 1, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
