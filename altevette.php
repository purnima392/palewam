<?php include('header.php')?>
<section class="content inner-content">
	<div class="container">
		
		
		
		<div class="row mb-20">
			<div class="col">
				<h2 class="innertitle">Altevette Onlus(Italy) </h2>
			</div>
			<div class="col text-right">
				<a href="index.php" class="breadcrumb">
					<i class="fa fa-home"></i> Back to home
				</a>
				
			</div>
		</div>
		<p><strong>Altevette Onlus Organization</strong>  is from Italy and they are looking after our <strong>Nunnery school</strong> mainly. <strong>Mrs. Francesca Stengel</strong> is a <strong>President of Altevette Onlus</strong>. Since from <strong>2008</strong> our nuns got a huge support and care from <strong>Altevette Onlus.</strong> Teacher’s salary and fooding for Nunnery school is circulated by <strong>Altevette Onlus.</strong> At the moment we have 49 nuns and all the nuns have their own individual sponsors find by <strong>Altevette Onlus.</strong> 
</p>
		<div id="carasouel_gallery" style="display:none;">
			
			<a href="#">
				<img alt="image"
				src="img/23.jpg"
				data-image="img/23.jpg"
				data-description=""
				style="display:none">
			</a>
			
			<a href="#">
				<img alt="image"
				src="img/27.jpg"
				data-image="img/27.jpg"
				data-description=""
				style="display:none">
			</a>
			
			<a href="#">
				<img alt="image"
				src="img/31.jpg"
				data-image="img/31.jpg"
				data-description=""
				style="display:none">
			</a>
			
			<a href="#">
				<img alt="image"
				src="img/40.jpg"
				data-image="img/40.jpg"
				data-description=""
				style="display:none">
			</a>
		</div>
		
		
		<p><b>List of Sponsors of nuns</b></p><br>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>S.No.</th>
					<th>Name</th>
					<th>Class</th>
					<th>Sponsor</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1.</td>
					<td>Karma Yangzom</td>
					<td>One</td>
					<td>Grazia Sacchi</td>
				</tr>
				<tr>
					<td>2.</td>
					<td>Ngawang Palmo</td>
					<td>One</td>
					<td>Deborah Wooden USA</td>
				</tr>
				<tr>
					<td>3.</td>
					<td>Sonam Sangmo</td>
					<td>One</td>
					<td>Gaia Scov. Heidi Ceffa</td>
				</tr>
				<tr>
					<td>4.</td>
					<td>Tsering Tsomo</td>
					<td>One</td>
					<td>Donatella Peracchi Pino</td>
				</tr>
				<tr>
					<td>5.</td>
					<td>Tsering Lhamo</td>
					<td>Two</td>
					<td>Susan Muir Austrlia</td>
				</tr>
				<tr>
					<td>6.</td>
					<td>Ngawang Choedon</td>
					<td>Two</td>
					<td>Preeya Sibunrauang, Thailand</td>
				</tr>
				<tr>
					<td>7.</td>
					<td>Choeying Dolma</td>
					<td>Two</td>
					<td>Diane Hatwell, Austrlia</td>
				</tr>
				<tr>
					<td>8.</td>
					<td>Ngawang Choezom</td>
					<td>Two</td>
					<td>Mariagrazia Ferro</td>
				</tr>
				<tr>
					<td>9.</td>
					<td>Nyinda Dolma</td>
					<td>Two</td>
					<td>Susann Kiehne, Germany</td>
				</tr>
				<tr>
					<td>10.</td>
					<td>Jamyang Dolma</td>
					<td>Two</td>
					<td>Marzia, Monica, Elisabetta</td>
				</tr>
				<tr>
					<td>11.</td>
					<td>Sonam Dolma</td>
					<td>Two</td>
					<td>Marie-Pierre Schickel</td>
				</tr>
				<tr>
					<td>12.</td>
					<td>Gyaltsen Dolma</td>
					<td>Two</td>
					<td>Francesca Rossi</td>
				</tr>
				<tr>
					<td>13.</td>
					<td>Jamyang Yangchen</td>
					<td>Two</td>
					<td>Fereidoun Nassiri</td>
				</tr>
				<tr>
					<td>14.</td>
					<td>Tseyang Lahmo</td>
					<td>Two</td>
					<td>Katia, Anna and Silivio</td>
				</tr>
				<tr>
					<td>15.</td>
					<td>Tsering Youdon(Youten)</td>
					<td>LKG</td>
					<td>Cinzia Lombardi</td>
				</tr>
				<tr>
					<td>16.</td>
					<td>Tamdin Wangmo</td>
					<td>LKG</td>
					<td>Giovanna Milanesi Galbiati</td>
				</tr>
				<tr>
					<td>17.</td>
					<td>Pema Yangzom</td>
					<td>UKG</td>
					<td>Immacolata Sgalambro</td>
				</tr>
				<tr>
					<td>18.</td>
					<td>Sonam Yangzom</td>
					<td>UKG</td>
					<td>Ilara Cardelli Santucci</td>
				</tr>
				<tr>
					<td>19.</td>
					<td>Lhenzen</td>
					<td>LKG</td>
					<td>Irene Ceffa</td>
				</tr>
				<tr>
					<td>20.</td>
					<td>Tsering Wangmo</td>
					<td>LKG</td>
					<td>Maria Collari</td>
				</tr>
				<tr>
					<td>21.</td>
					<td>Norzin Wangmo</td>
					<td>UKG</td>
					<td>Mary Kay</td>
				</tr>
				
				<tr>
					<td>22.</td>
					<td>Ngawang Lahmo’B’</td>
					<td>LKG</td>
					<td>Alessandra Bruti Liberati</td>
				</tr>
				<tr>
					<td>23.</td>
					<td>Ngawang Lahmo’A’</td>
					<td>UKG</td>
					<td>Michelae Davide Petroni</td>
				</tr>
				<tr>
					<td>24.</td>
					<td>Jamyang Choeozm</td>
					<td>Six</td>
					<td>Fereidoun Nassiri</td>
				</tr>
				<tr>
					<td>25.</td>
					<td>Ngawang Dickyi</td>
					<td>Six</td>
					<td>Massimo Bonomelli</td>
				</tr>
				<tr>
					<td>26.</td>
					<td>Ngawang Tsomo</td>
					<td>Four</td>
					<td>Laura Perolfi</td>
				</tr>
				<tr>
					<td>27.</td>
					<td>Choekden Dolma</td>
					<td>Four</td>
					<td>Michele Maria Letizia Di Tuccio</td>
				</tr>
				<tr>
					<td>28.</td>
					<td>Ngawang Lhazee(Lhazom)</td>
					<td>Four</td>
					<td>Linda Ambrose</td>
				</tr>
				<tr>
					<td>29.</td>
					<td>Sonam Palmo</td>
					<td>Four</td>
					<td>Carla Gemelli</td>
				</tr>
				<tr>
					<td>30.</td>
					<td>Yangchen Dolkar</td>
					<td>Three</td>
					<td>Emanuela e Aldo Fratoni</td>
				</tr>
				<tr>
					<td>31.</td>
					<td>Nyinda Wangmo(Sita)</td>
					<td>Three</td>
					<td>Mair Eiwen & Michael Barby UK</td>
				</tr>
				<tr>
					<td>32.</td>
					<td>Yangchen Dickyi</td>
					<td>Three</td>
					<td>Giusi Perego</td>
				</tr>
				<tr>
					<td>33.</td>
					<td>Ngawang Lhazey(Rabina)</td>
					<td>Three</td>
					<td>Gianfranco e Flavia Cesaretti</td>
				</tr>
				<tr>
					<td>34.</td>
					<td>Jamyang Palmo</td>
					<td>Three</td>
					<td>Claudia Gemelli Uboldi</td>
				</tr>
				<tr>
					<td>35.</td>
					<td>Kunsang Wangmo</td>
					<td>LKG</td>
					<td>Ettore Barbieri</td>
				</tr>
				<tr>
					<td>36.</td>
					<td>Yangchen Palmo</td>
					<td>Five</td>
					<td>Claudia Borri</td>
				</tr>
				<tr>
					<td>37.</td>
					<td>Sonam Dickyi</td>
					<td>Five</td>
					<td>Vittoria e Franco Primavesi</td>
				</tr>
				<tr>
					<td>38.</td>
					<td>Ngwawang Dolma</td>
					<td>Five</td>
					<td>Barbara Pozzo Linda Luciano</td>
				</tr>
				<tr>
					<td>39.</td>
					<td>Jamyang Tsomo</td>
					<td>Five</td>
					<td>Roberto e Elena Sampietro</td>
				</tr>
				<tr>
					<td>40.</td>
					<td>Tashi Lhamo</td>
					<td>UKG</td>
					<td>Cindy Stewart</td>
				</tr>
				<tr>
					<td>41.</td>
					<td>Lhenzom Gurung</td>
					<td>UKG</td>
					<td>Joan Ryan</td>
				</tr>
				<tr>
					<td>42.</td>
					<td>Tenzin Wangmo</td>
					<td>LKG</td>
					<td>Aliza Milizia</td>
				</tr>
				<tr>
					<td>43.</td>
					<td>Namkha Dolma</td>
					<td>Six</td>
					<td>Bruna Penco</td>
				</tr>
				<tr>
					<td>44.</td>
					<td>Rinzin Lhamo</td>
					<td>Six</td>
					<td>Paolo Tortorella</td>
				</tr>
				<tr>
					<td>45.</td>
					<td>Khunga Lhamo</td>
					<td>Six</td>
					<td>Elisabetta Illi Petrone</td>
				</tr>
				<tr>
					<td>46.</td>
					<td>Lhamo Gurung</td>
					<td>Six</td>
					<td>Carmeila Ricciardi</td>
				</tr>
				
				
				
			</tbody>
		</table>
		
		
		
		
	</div>
</section>






<?php include('footer.php')?>
<script type='text/javascript' src='js/unitegallery.min.js'></script> 
<script type='text/javascript' src='js/ug-theme-carousel.js'></script>
<script type="text/javascript">

		jQuery(document).ready(function(){

			jQuery("#carasouel_gallery").unitegallery();

		});
		
	</script>
