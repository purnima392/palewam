<?php include('header.php')?>
<section class="content inner-content">
  <div class="container">
    <div class="row mb-20">
      <div class="col">
        <h2 class="innertitle">Namgon Welfare Committee</h2>
      </div>
      <div class="col text-right"> <a href="index.php" class="breadcrumb"> <i class="fa fa-home"></i> Back to home </a> </div>
    </div>
    <p>Namgon welfare committee was formed on 10th July 2016 under the approval of Ven. Khenpo Tsewang Rigzin la (Founder of Pal Ewam Namgyal Monastic School and Pal Ewam Namgon Nunnery School). The welfare members are from staffs of PENMS and students. The committee’s main objective is to form to maintain the firm management of the school and to look after the future planning of the school. Namgon welfare committee will take the whole and initiative responsibility of the school and the student’s academic progress.</p>
    <table class="table table-bordered mt-20">
      <thead>
        <tr>
          <th width="5%">S.N.</th>
          <th>Name</th>
          <th>Designation</th>
          <th>Address</th>
          <th>Phone</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1.</td>
          <td>Ven. Khenpo Khenrab Sangpo </td>
          <td>President </td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        <tr>
          <td>2.</td>
          <td>Ven. Pema Wangdi Gurung</td>
          <td>V.President </td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        <tr>
          <td>3.</td>
          <td>Mr.Sonam Choedup</td>
          <td>Secretary  </td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        <tr>
          <td>4.</td>
          <td>Ven.Gyatso Gurung</td>
          <td>Treasurer</td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        <tr>
          <td>5.</td>
          <td>Mr. Tsering Namgyal Thakuri</td>
          <td>Accountant</td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        <tr>
          <td>6.</td>
          <td>Ven Tenpa Gyatso</td>
          <td>Member</td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        <tr>
          <td>7.</td>
          <td>Ms. Yangchen Gurung</td>
          <td>Member</td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        <tr>
          <td>8.</td>
          <td>Ngawang Dickyi</td>
          <td>Member</td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        <tr>
          <td>9.</td>
          <td>Ngawang Choemphel</td>
          <td>Member</td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
      </tbody>
    </table>
    <h2 class="innertitle">Aims and Objective</h2>
    <ul class="list">
    	<li>The welfare committee aims to provide and manitna a caring, safe and secure environment to all the student and staffs.</li>
        <li>Ensuring all the students and teachers has an equal access to the curriculum and extra curriculum opportunists.</li>
        <li>To enable each child to reach their full potential by fostering self esteem and sense of achievement.
</li>
        <li>To arrange the teacher counseling and training program if necessary.</li>
        <li>To make sure that the students are in sound and to build up their mind strong.</li>
        <li>To meet all the legal and other obligations places upon them by virtue of the office.
</li>
        <li>To make and organize a fund raise programme as many as possible.
</li>
        <li>To maintain the equal love and respect to all the students and staffs.
</li>
<li>To guaranteeing the school administration and the future planning of the school.</li>
    </ul>
  </div>
</section>
<?php include('footer.php')?>