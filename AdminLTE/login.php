<?php
   include("inc/config.php");
   session_start();
   $error='';
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      
      $myusername = $_POST['username'];
      $mypassword = $_POST['password']; 
      
      $sql = $mysqli->query("SELECT * FROM cp_admin WHERE Username = '$myusername' and Password = '$mypassword'");
      
      $count = $sql->num_rows;
      
      // If result matched $myusername and $mypassword, table row must be 1 row
		
      if($count == 1) {
         $_SESSION['login_user'] = $myusername;
         header("location: index.php");
      }else {
         $error = "Your Login Name or Password is invalid";
      }
   }
?>
<html>
<head>
<title>Login Page</title>
<style type = "text/css">

body {
    font-family: Arial,Helvetica,sans-serif;
    font-size: 14px;
}
label {
    font-size: 14px;
    font-weight: normal;
    width: 100px;
}
.box {
    border: 1px solid #666666;
}
.login-boxwrap {
    height: 100%;
    position: relative;
}
.login-box {
    height: 250px;
    left: 50%;
    margin-left: -175px;
    margin-top: -125px;
    max-width: 350px;
    position: absolute;
    top: 50%;
}
.login-box h2 {
    background-color: #c71616;
    color: #fff;
    font-size: 15px;
    font-weight: normal;
    margin: 0;
    padding: 8px 15px;
    text-align: left;
    text-transform: uppercase;
}
.login-box form {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: currentcolor #ccc #ccc;
    border-image: none;
    border-style: none solid solid;
    border-width: 0 1px 1px;
    padding: 15px;
}
.login-box form label {
    display: inline-block;
    margin-right: 15px;
    width: 78px;
}
.login-box form .box {
    background-color: #fff;
    border: 1px solid #ccc;
    height: 30px;
    margin-bottom: 15px;
    padding: 5px 15px;
}
.login-box form input[type="submit"] {
    background-color: #191919;
    border: 1px solid #0e0e0e;
    color: #fff;
    display: block;
    padding: 7px 15px;
    text-transform: uppercase;
}
.error_message {
    background-color: #c71616;
    color: #fff;
    font-size: 12px;
    font-style: italic;
    padding: 5px 15px;
}

</style>
</head>

<body bgcolor = "#FFFFFF">
<div class="login-boxwrap">
  <div class="login-box">
    <h2><b>Login</b></h2>
    <div>
      <form action = "" method = "post">
        <label>UserName  :</label>
        <input type = "text" name = "username" class = "box"/>
       
        <label>Password  :</label>
        <input type = "password" name = "password" class = "box" />
       
        <input type = "submit" value = " Submit "/>
     
      </form>
      <div class="error_message"><?php echo $error; ?></div>
      
    </div>
  </div>
</div>
</body>
</html>