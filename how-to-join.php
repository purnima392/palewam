<?php include('header.php')?>
<section class="content inner-content">
  <div class="container">
    <div class="row mb-20">
      <div class="col">
        <h2 class="innertitle">Volunteer Programme
</h2>
      </div>
      <div class="col text-right"> <a href="index.php" class="breadcrumb"> <i class="fa fa-home"></i> Back to home </a> </div>
    </div>
    <p>Pal Ewam Namgyal Monastic School and Pal Ewam Namgon Nunnery School welcome to all the volunteers. Please read carefully for participating in the volunteer program offer by Pal Ewam Namgyal Monastic School. By submitting your online registration form and email communication by PENMS, you confirm that you acknowledge and accept the following terms and conditions. We suggest you that during the month of April and October, our vocation falls on that month so we kindly request all the volunteers that during that month we will not accept the volunteers. We have attached the volunteer Registration form in our website and you can apply a volunteer in our school online also.</p>
    <ul class="list">
    	<li>If you are going to visit the outside  area by yourself you will you have to inform to the school office and return back to school before 9:00 pm.</li>
        <li>We would to love to have a matured volunteer if possible.</li>
        <li>The cost of our volunteer program, if he/she is staying more than one month per day we charge 2000 NPR and if/she is staying less than one month we charge 2500 NPR per day.</li>
        <li>Your volunteer fee includes three time meal, accommodation, WIFI, hot and cold shower and hot and cold water in your room.</li>
        <li>Volunteers are requested to wear decent clothes in the school compound, especially to the ladies. Short skirt and short paint are not allowed to wear during the school hour.</li>
        <li>If you are not having a meal further notice should be given to the school office.</li>
        <li>All the volunteers are welcome in our school during your stay with us.</li>
        <li>Smoking and drinking inside the school compound is strictly prohibited.</li>
        <li>You are not allowed to bring the outside in your room without taking permission from the school office. We are not responsible if there is anything lost or theft from your room.</li>
        <li>Every Saturday we have a half and on Sunday we have a full holiday, so if you love to go outside for short trekking you can plan on this days. But you must be present on the Monday morning for the class.</li>
        <li>If you wish to help our students, you can buy stationaries stuffs and games and sports equipment.</li>
        <li>If you want to extend your day in our school kindly inform to our school 3 days earlier.</li>
        <li>You confirm that  you have read all of the information available on our website about the aim of projects, activities, accommodation costs, and terms of participating. You agree that you respect the politics of the school and rules of our school in which you are staying.</li>
    </ul>
  </div>
</section>
<?php include('footer.php')?>