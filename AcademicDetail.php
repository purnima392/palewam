<?php include('header.php');
	require('AdminLTE/inc/config.php');
	$AcademicId=$_GET['id'];
	$latProjects=$mysqli->query("select * from academics where AcademicId=$AcademicId");
          $SiPackage=$latProjects->fetch_array();
	$Title=$SiPackage["Title"];
	$Description=$SiPackage["Description"];
          $Photo=$SiPackage["Photo"];
?>
<section class="content inner-content">
	<div class="container">
		<div class="row mb-20">
			<div class="col">
				<h2 class="innertitle"><?=$Title?></h2>
			</div>
			<div class="col text-right">
				<a href="index.php" class="breadcrumb">
					<i class="fa fa-home"></i> Back to home
				</a>
				
			</div>
		</div>
		<div class="project project-detail">
			<div class="project_img"><img src="img/<?=$Photo?>"></div>
			<div class="project-content">
				<h3><?=$Title?></h3>
				<?=$Description?>
			</div>
		</div>
		
		
		
		
	</div>
</section><?php include('footer.php')?>