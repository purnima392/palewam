<?php

if (isset($_FILES['files']) && !empty($_FILES['files'])) {
         define ("MAX_SIZE","55000");
         function getExtension($str) {
         $i = strrpos($str,".");
         if (!$i) { return ""; }
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
        } 
        $filename = stripslashes($_FILES['files']['name']);    
        $extension = getExtension($filename);
        $extension = strtolower($extension);  
        if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png")) 
        {         
            echo "<script>alert('Please choose image file to upload');</script>";
            echo '<span>Upload Photo</span><input type="file" id="txtUploadPhoto" name="txtUploadPhoto">';
        }
        else{
        $size=filesize($_FILES['files']['tmp_name']); 
        if ($size > MAX_SIZE*5024)
        {
          echo "<script>alert('You have exceeded the size limit!');</script>";
          echo '<span>Upload Photo</span><input type="file" id="txtUploadPhoto" name="txtUploadPhoto">';
        }
        else{
            move_uploaded_file($_FILES["files"]["tmp_name"], 'img/' . $_FILES["files"]["name"]);
            $filename=$_FILES["files"]["name"];
            $html='<img src="img/';
            $html.=$filename;
            $html.='">'; 
            $html.='<input type="file" id="txtUploadPhoto" name="txtUploadPhoto">';
            echo $html;
        }
    }
    }
else {
    echo 'Please choose at least one file';
}
    
/* 
 * End of script
 */