 $(document).ready(function(){
    $("#txtArrivalDate").datepicker();
    $("#txtDepartureDate").datepicker();
   
   $('#divUploadImage').on('change', '#txtUploadPhoto',function() {
    var file_data = $('#txtUploadPhoto').prop('files')[0];  
    var filename=$('#txtUploadPhoto').prop('files')[0]['name'];
    $('#hdnPhoto').val(filename); 
    var form_data = new FormData();                  
    form_data.append('files', file_data);
    $.ajax({
                url: 'PhotoUpload.php', // point to server-side PHP script 
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(response){                    
                   $('#divUploadImage').html(response);
                }
     });
    });

      
   
 var doc = new jsPDF('p','pt','a4');
var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};
$("#btnPdf").on('click',function(){   
    doc.fromHTML($('#divRegistrationForm').html(),0.5,0.5, {
        'width': 300,
            'elementHandlers': specialElementHandlers
    });
    doc.save('sample-file.pdf');
});

// This code is collected but useful, click below to jsfiddle link.

});