<?php include('header.php');
	require('AdminLTE/inc/config.php');
	$ProjectId=$_GET['id'];
	$latProjects=$mysqli->query("select * from projects where ProjectId=$ProjectId");
	$SiPackage=$latProjects->fetch_array();
	$ProjectId=$SiPackage["ProjectId"];
	$ProjectName=$SiPackage["ProjectName"];
	$Description=$SiPackage["Description"];
?>
<section class="content inner-content">
	<div class="container">
		<div class="row mb-20">
			<div class="col">
				<h2 class="innertitle"><?=$ProjectName?></h2>
			</div>
			<div class="col text-right">
				<a href="index.php" class="breadcrumb">
					<i class="fa fa-home"></i> Back to home
				</a>
				
			</div>
		</div>
		<div class="project project-detail">
			<div class="row">
				<?php if($ProjectId==11){ ?>
					<div class="col-lg-4 col-md-4">
						<div class="project_img">
							<video width="400" controls>
								<source src="video/1503940811650.mp4" type="video/mp4">
								<source src="video/1503940811650.ogg" type="video/ogg">
							</video>
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="project_img">
							<video width="400" controls>
								<source src="video/1505381367362.mp4" type="video/mp4">
								<source src="video/1505381367362.ogg" type="video/ogg">
							</video>
						</div>
					</div>
				<?php } ?>
				<?php
					$latImage=$mysqli->query("select * from images where ProjectId=$ProjectId");
					while($SiImage=$latImage->fetch_array()){
						$Photo=$SiImage["Image"];
					?>	
					
					<div class="col-lg-4 col-md-4">
						<div class="project_img"><img src="img/<?=$Photo?>"></div>
					</div>
					
					
				<?php } ?>
			</div>
			<div class="project-content">
				<h3><?=$ProjectName?></h3>
				<?=$Description?>
			</div>
		</div>
		
		
		
		
	</div>
</section>






<?php include('footer.php')?>