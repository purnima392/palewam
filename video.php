<?php include('header.php');
      require('AdminLTE/inc/config.php');
?>
<section class="content inner-content">
	<div class="container">
		<div class="row mb-20">
			<div class="col">
				<h2 class="innertitle">Video</h2>
			</div>
			<div class="col text-right">
				<a href="index.php" class="breadcrumb">
					<i class="fa fa-home"></i> Back to home
				</a>
				
			</div>
		</div>
		<div id="gallery" style="display:none;">
		<?php
            $latProjects=$mysqli->query("SELECT * FROM video");
            while($SiPackage=$latProjects->fetch_array()){
			$VideoId=$SiPackage["VideoId"];
			$Title=$SiPackage["Title"];
			$VideoLink=$SiPackage["VideoLink"];
	    ?>
		 <img alt="<?=$Title?>"
		     data-type="youtube"  src="https://img.youtube.com/vi/<?=$VideoLink?>/sddefault.jpg"
		     data-image="https://img.youtube.com/vi/<?=$VideoLink?>/sddefault.jpg"
		     data-description="<?=$Title?>"
		     data-videoid="<?=$VideoLink?>" style="display:none">
		<?php } ?>
		</div>
	</div>
</section>

<?php include('footer.php')?>
<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery("#gallery").unitegallery({
				gallery_width:"960",
				tile_enable_border:true,
				tile_border_color:"#ffffff",
				tile_enable_outline:true,
				tile_outline_color:"#b6b6b6",
				tile_shadow_color:"#8B8B8B",
				tile_overlay_opacity:0.6,
				tile_enable_image_effect:true,
				tile_image_effect_type:"blur",
				tile_image_effect_reverse:true,
				tile_enable_textpanel:true,
				tile_textpanel_bg_color:"#332e68",
				tile_textpanel_bg_opacity:0.9,
				tile_textpanel_title_text_align:"center",
				lightbox_textpanel_enable_title:false,
				lightbox_textpanel_enable_description:true,
				lightbox_textpanel_desc_color:"e5e5e5",
				tiles_col_width:200,
				tiles_space_between_cols:30	
			});

		});
		
	</script>