<?php include('header.php')?>
<section class="content inner-content">
  <div class="container">
    <div class="row mb-20">
      <div class="col">
        <h2 class="innertitle">Contact Us</h2>
      </div>
      <div class="col text-right"> <a href="index.php" class="breadcrumb"> <i class="fa fa-home"></i> Back to home </a> </div>
    </div>
    <div id="map"></div>
    <div class="row">
      <div class="col-lg-6 col-md-6 mt-20">
        <div class="contact_address">
          <h2 class="innertitle">Pal Ewam Namgyal Monastic School</h2>
          <ul>
            <li><i class="fa fa-map-marker"></i><b>Address : </b> Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal </li>
            <li><i class="fa fa-phone"></i><b>Phone : </b> +977-061-622629 </li>
            <li><i class="fa fa-envelope"></i><b>Email : </b> <a href="mailto:info@penms.edu.np">info@penms.edu.np</a> </li>
            <li><i class="fa fa-globe"></i><b>Website : </b> <a href="http://penms.edu.np/" target="_blank">www.penms.edu.np</a>
</li>
            
          </ul>
          <hr>
          <h2 class="innertitle">Address of Mustang</h2>
          <ul>
            <li><i class="fa fa-map-marker"></i><b>Address : </b> Chhonup 1, Namgyal, Mustang, Nepal
</li>
            <li><i class="fa fa-phone"></i><b>Phone : </b> +977-061-622629 </li>
            <li><i class="fa fa-envelope"></i><b>Email : </b> <a href="mailto:info@penms.edu.np">info@penms.edu.np</a> </li>
            <li><i class="fa fa-globe"></i><b>Website : </b> <a href="http://penms.edu.np/" target="_blank">www.penms.edu.np</a>
</li>
            
          </ul>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 mt-20">
      	 <h2 class="innertitle">Get In Touch</h2>
         <p class="lead">Please fill up the required(<span class="red">*</span>) field.</p>
         <div class="contact-form">
			
			<form class="row">
			<div class="form-group col-lg-6 ">
    <label for="exampleInputName">Name</label>
    <input type="text" class="form-control" id="exampleInputName" aria-describedby="name">
    
  </div>
  <div class="form-group col-lg-6 ">
    <label for="exampleInputEmail">Email</label>
    <input type="email" class="form-control" id="exampleInputEmail" aria-describedby="email">
    
  </div>
  <div class="form-group col-lg-6 ">
    <label for="exampleInputphone">Phone No.</label>
    <input type="text" class="form-control" id="exampleInputphone" aria-describedby="phone">
    
  </div>
  <div class="form-group col-lg-6 ">
    <label for="exampleInputsubject">Subject</label>
    <input type="text" class="form-control" id="exampleInputsubject" aria-describedby="subject">
    
  </div>
 
 <div class="form-group col-lg-12 ">
    <label for="exampleFormControlTextarea1">Message</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
  <div class="form-group col-lg-6 "><button type="submit" class="btn btn-primary">Submit</button></div>
</form>	
		
		</div>
      </div>
    </div>
  </div>
</section>
<?php include('footer.php')?>